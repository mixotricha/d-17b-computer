# D-17B Computer

This is a work in progress to restore an emulation of the D-17B Minute Man computer written 
in Fortran in a thesis from AFIT in 1972. Thanks to the help of the AFIT a new scan has been 
obtained though it also has some problems. The original scan was of low resolution making the 
source code impossible to read but it was character complete. The new scan is easier to read but some of the characters are missing or unreadable and the pages are distorted. I hope that between the two documents I can recover a complete listing of the code by building a ground truth and using some OCR as well as other recovery techniques. 

More information about the D17B computer is available here : [D17B Computer](https://www.repairfaq.org/sam/other/d17b/)

Rescanned document by AFIT is available here : [DB17B Simulation](https://scholar.afit.edu/etd/6893/)



