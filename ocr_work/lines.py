from PIL import Image

from PIL import ImageDraw

import pytesseract 

#page_3_header=['output-113.png',225,0.35]
#page_3_lines=[167,168,169,173,178,182,183,186,188,189,190,191,192,196,197,198,200,201,207,208,209,210,212,213,221,224,225,226,227,229,231,233,235,237,238,239,240,245,246]
#page_3_correct = [0,0,0,2,5,8,6,6,8,8,8,9,9,11,11,11,12,12,17,17,17,17,19,19,22,22,22,25,25,25,28,28,28,28,28,28,28,32,34,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]

#page_4_header = ['output-114.png',229,0.0,'page_4.png']
#page_4lines=[249,250,251,252,253,255,257,258,259,261,263,266,267,268,271,272,273,274,275,276,277,278,279,280,281,282,283,285,287,289,290,291,292,293,295,296,297,300,303,305,309,314,315,316,319,321,323,324,325,326,327,328,330,331]
#page_4_correct=[0,0,1,1,2,2,2,5,6,6,7,7,8,8,8,9,10,11,11,11,12,12,13,13,12,15,15,16,18,20,20,20,20,20,20,20,22,22,24,24,28,28,28,31,31,33,33,33,33,33,33,35,35,37]

header = ['output-115.png',209,0.05,'page_05.png']
lines=[
332,333,334,335,336,
337,338,340,343,344,
346,347,348,352,354,
355,356,357,358,360,
361,365,366,367,371,
373,374,375,378,379,
381,382,384,385,387,
388,390,391,394,396,
397,398,399,400,401,
402,403,404,405,406,
409,410,413,414
]


correct=[
0,0,0,0,0,
3,2,4,4,4,
6,6,6,6,6,
6,6,6,9,9,
9,11,12,12,14,
15,17,17,18,18,
19,19,21,21,23,
24,24,24,24,26,
26,27,28,28,28,
29,29,30,30,30,
33,33,33,35,35,
0,0,0,0,0
] 

# Open the image file

img = Image.open(header[0])
img = img.rotate(header[2], expand=True)
text_starts = header[1] 
start = lines[0] 
ii = 0 
x1 = 100
x2 = x1 + 1200
y1 = text_starts

new_img = Image.new('RGB', (max(x2, x1 + 1200), y1 + 16 * len(lines)), (50, 255, 255))

for i in lines:
	print(i,ii) 
	if (i != start ) : 
		y1 = y1+(16*(i-start))
	y2 = y1 + 16
	rect = img.crop((x1, y1+correct[ii], x2, y2+correct[ii]))
	
	ImageDraw.Draw(rect).line([(0, rect.height-1), (rect.width, rect.height-1)], fill=(254, 0, 0))
	
	custom_config = r'--oem 3 --psm 6'
	result = pytesseract.image_to_string(rect, config=custom_config)
	print(result) 
	rect.save('a_'+str(i)+'.png')
	new_img.paste(rect, (x1, ii*16))
	y1 = text_starts  
	ii=ii+1

new_img.save(header[3])
 