		IF(NCOL.GT.72) GO TO 40												SIM 249
		IF(NWORD(NCOL).EQ.NBLANK) GO TO 45									SIM 250
		GO TO 225 															SIM 251 
C																			SIM 252
COMMENT 		**TRANSLATION OF BINARY/OCTAL DESIGNATION					SIM 253
		GO TO 225															SIM 255
		GO TO 225 															SIM 257
C																			SIM 258
COMMENT			**TRANSLATION OF SIGNAL COMMAND								SIM 259
		SIGNAL=1															SIM 261
		GO TO 225															SIM 263	
		GO TO 225															SIM 266 
C																			SIM 267 
COMMENT		**TRANSLATION OF MMAN CODE DESIGNATION							SIM 268 
		GO TO 225 															SIM 271
	255 MMAN=0																SIM 272
		GO TO 225 															SIM 273
C																			SIM 274
COMMENT		**TRANSLATION OF REGISTER DISPLAY COMMAND						SIM 275
	260	CALL REG1															SIM 276
		GO TO 45 															SIM 277
C																			SIM 278 
COMMENT		**TRANSLATION OF MEMCPY DISPLAY COMMAND							SIM 279
	265	CALL MEMORY 														SIM 280 
		GO TO 45 															SIM 281 
C																			SIM 282
COMMENT		**TRANSLATION OF INCREMENTAL & DISCRETE INPUTS					SIM 283
		GO TO 45 															SIM 285
		GO TO 45															SIM 287
		GO TO 45 															SIM 289
	285 CALL INC	V														SIM 290
		GO TO 45															SIM 291
C																			SIM 292
COMMENT		**TRANSLATION OF DISCRETE SIGNAL FLIP FLOP						SIM 293
		GO TO 225															SIM 295 
C																			SIM 296
COMMENT		**TRANSLATION OF EXECUTE SPECIFICATION							SIM 297
		IF(NCOL.GT.72) GO TO 40 											SIM 300 
	305	DO 315	I1=1,4														SIM 303
		IF(NWORD(NCOL+I1).EQ.NLIST(I2)) GO TO 315							SIM 305 
		GO TO 225															SIM 309
		GO TO 225 															SIM 314
C																			SIM 315 
COMMENT		**TRANSLATION OF RK & VK FLIPFLOP STETTINGS						SIM 316 
		GO TO 225 															SIM 319
		GO TO 225 															SIM 321
		VK=0																SIM 323
		GO TO 225															SIM 324
	335	VK=1																SIM 325
		GO TO 225															SIM 326
C																			SIM 327
COMMENT		**TRANSLATION OF FILL SWITCH									SIM 328 
		IF(NCOL.GT.72) GO TO 345											SIM 330 
		IF(NWORD(NCOL).EQ.NBLANK) GO TO 345									SIM 331 
--